Group 1 - 3C15. Topic: Fourier Transform

Member: Nguyễn Như Tuấn Hiệp, Nguyễn Thị Thanh Hoa, Lê Thị Thảo, Đặng Ngọc Thúy

<h1>Week 8: Testing code, upload image and code on Gitlab, test basic command on Gitlab</h1>
-------------------------------------------------------------------------------------------------------------------------------------------------------------
<h1>Week 9: Outline of report:</h1>

    -Part I: Theories of Fourier Transform
    
    -Part II: Image Filtering in Frequency Domain
    
    -Part III: Image Smoothing Using Frequency Domain Filters
    
-------------------------------------------------------------------------------------------------------------------------------------------------------------
 <h1>Week 10: Finish part I: Theories of Fourier Transform: </h1>
 
    -Fourier Series
    
    -Definition of Fourier Transform
    
    -Two-Dimensional Fourier Transform
    
    -Convolution
    
Uploaded part I as file named Fourier_Transform.docx in topic_direction folder
              
Begin part II: Image Filtering in Frequency Domain.      
        
        
-------------------------------------------------------------------------------------------------------------------------------------------------------------
 <h1>Week 11: Finish part II: Image Filtering in Frequency Domain: </h1>
 
    - Frequency Domain Filtering Fundamentals
    - Summary of Steps for Filtering in the Frequency Domain

Uploaded part II as file named Image_Filtering.docx in topic_direction folder

Begin part III: Image Smoothing Using Frequency Domain Filters

-------------------------------------------------------------------------------------------------------------------------------------------------------------
 <h1>Week 12: Finish part III: Image Smoothing Using Frequency Domain Filters: </h1>
    - Ideal Lowpass Filter
    - Butterworth Lowpass Filter
    - Gaussian Lowpass Filter

Uploaded part III as file Filtering_Technique.docx in topic_direction folder


